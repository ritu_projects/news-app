/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React,{Component} from 'react';
 import { AppRegistry, } from 'react-native';

 
 import Routes from './Routes.js'
 
 
 class App extends Component {
   constructor() {
     super();
   }
 
 
 
 
 render() {
  console.disableYellowBox = true;

   return (
      <Routes />  
   )
 }
 }
 export default App
 AppRegistry.registerComponent('AwesomeProject3', () => AwesomeProject3)