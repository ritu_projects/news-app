import { Actions, Router, Stack, Scene } from 'react-native-router-flux';
import React from 'react';
import { BackHandler } from 'react-native'

import Home from './screens/Home'
import HomeDetails from './screens/HomeDetails'

_backAndroidHandler = () => {
  const scene = Actions.currentScene;
  
  Actions.pop();
  return true;
};


const Routes = () => (
  <Router
    navigationBarStyle={{ backgroundColor: '#8B008B', height: 45 }} tintColor='white'
    backAndroidHandler={this._backAndroidHandler}>
    <Stack key="root">
    <Scene
              component={Home}
              hideNavBar={true}
              key="SplashScreen"
              title="SplashScreen"
              initial={true}
              wrap={false}
            />

                <Scene
              component={HomeDetails}
              hideNavBar={true}
              key="HomeDetails"
              title="HomeDetails"
              wrap={false}
            />


    </Stack>

  </Router>
);
export default Routes;