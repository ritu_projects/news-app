import React from 'react';
import { StatusBar, Image, ActivityIndicator, FlatList, PixelRatio, TextInput, View, ImageBackground, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';

import styles from "../styles/styles";
import { Card } from 'native-base';
import { SafeAreaView } from 'react-native-safe-area-context';

import PopupDialog, {
    DialogTitle, DialogContent, DialogFooter, DialogButton, SlideAnimation, ScaleAnimation,
} from 'react-native-popup-dialog';
import { WebView } from 'react-native-webview'

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            animating: false,
            lodingDialog: false,
        };
    }
    componentDidMount() {
        this.setState({
            animating: true,
            lodingDialog: true,

        })


        fetch("https://newsapi.org/v2/sources?apiKey=d29d58aab88d4ea0b04ddb245a230068", {
            method: 'GET',

        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.status == "ok") {
                    this.setState({
                        data: responseData.sources,
                        animating: false,
                        lodingDialog: false,
                    });
                } else {
                    alert('No record')
                    this.setState({
                        animating: false,
                        lodingDialog: false

                    });
                }
            }
            )
    }


    LoadingIndicatorView() {
        return (<View style={{ flex: 1 }}>
            <ActivityIndicator color='#009b88' size='large' style={{ flex: 1, alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }} />
        </View>)
    }
    gotoNext = (item) => {
        Actions.push('HomeDetails', { 'item': item })

    }
    onMessage(event) {
        console.log('On Message')
        Alert.alert(
            'On Message',
            event.nativeEvent.data,
            [
                { text: 'OK' },
            ],
            { cancelable: true }
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.containerWhite}>
            <StatusBar
              translucent={true}
              backgroundColor={'#324048'}
              barStyle={'light-content'}
            />


                <View
                    style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>


                    <FlatList
                        keyExtractor={item => item.id}
                        data={this.state.data}
                        maxToRenderPerBatch={3}

                        renderItem={({ item }) => (
                            <TouchableOpacity
                                style={{ justifyContent: 'center', alignItems: 'center', marginTop: 5 }}
                            >
                                <Card style={{ width: "95%", justifyContent: 'space-between', alignItems: 'center', flexDirection: 'column', padding: 10, borderRadius: 15 }}>


                                    <TouchableOpacity style={{ width: '100%', flexDirection: 'column' }}
                                        onPress={() => this.gotoNext(item)}
                                    >
                                        {/* <WebView
                                              style={{ width: '100%', height: 150, }}
                                                source={{

                                                  uri: item.url

                                                }}
                                               renderLoading={this.LoadingIndicatorView}
                                                startInLoadingState={true}
                                                 onMessage={(event) => this.handleMessage(event)}
                                                javaScriptEnabled={true}
                                            /> */}
                                        <Image
                                            style={{ width: '100%', height: 150, }}
                                            source={{

                                                uri: 'https://us.123rf.com/450wm/alhovik/alhovik1709/alhovik170900031/86481591-breaking-news-background-world-global-tv-news-banner-design.jpg?ver=6'

                                            }}

                                        ></Image>
                                        <Text style={{ fontSize: 14, fontWeight: 'bold', paddingLeft: 10, marginTop: 20, }}>{item.description}</Text>
                                    </TouchableOpacity>
                                </Card>
                            </TouchableOpacity>
                        )}
                    />
                </View>


                <PopupDialog
                    onHardwareBackPress={() => { this.setState({ lodingDialog: false }) }}
                    width={0.3}
                    visible={this.state.lodingDialog}
                    dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
                    <DialogContent>
                        <View style={{ alignItems: 'center', }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                style={[{ height: 10, marginBottom: 10, marginTop: 30, marginLeft: 20, marginRight: 20 }]}
                                color="#C00"
                                size="large"
                                hidesWhenStopped={true}
                            />
                        </View>
                    </DialogContent>
                </PopupDialog>
            </SafeAreaView>
        );
    }
}
