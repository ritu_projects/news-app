import React from 'react';
import { StatusBar, Image, Share, ScrollView, Platform, View, PermissionsAndroid, Text, Dimensions, TouchableOpacity, Linking, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Card } from 'native-base';
import styles from "../styles/styles";
const { width, height } = Dimensions.get("window");
import RNFetchBlob from 'rn-fetch-blob';


export default class HomeDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            animating: false,
            lodingDialog: false,
        };
    }
    componentDidMount() {


    }
    shareDetails = () => {
        console.log('item is ', this.props.item.url)

        const shareOptions = {
            title:'Share App',
            message: this.props.item.url, 
            url: this.props.item.url,
            subject: this.props.item.description,
          };
          Share.share(shareOptions);

    
    }
  
    onPermission = (url) => {
        var that = this;
        //Checking for the permission just after component loaded
        async function requestStoragePermission() {
            //Calling the permission function
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    title: 'Authorisation for the gallery',
                    message: 'The request for authorisation requires access to your gallery',
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                that.downloadImage(url);
            } else {
                alert('CAMERA Permission Denied.');
            }
        }
        if (Platform.OS === 'android') {
            requestStoragePermission();
        } else {
            that.downloadImage(url);
        }
    };

    downloadImage = (url) => {
        var date = new Date();
        console.log("URL is  " + url)
        var ext = this.getExtention(url);
        ext = "." + ext[0];
        const { config, fs } = RNFetchBlob;
        let PictureDir = fs.dirs.PictureDir
        let options = {
            fileCache: true,
            addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                path: PictureDir + "/image_" + Math.floor(date.getTime()
                    + date.getSeconds() / 2) + ext,
                description: 'Image'
            }
        }
        config(options).fetch('GET', url).then((res) => {
            console.log('res -> ', JSON.stringify(res));

        alert("Image Saved succesfully");
        });
    }
    getExtention = (filename) => {
        return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) :
            undefined;
    }

    render() {
        return (
            <SafeAreaView style={styles.containerWhite}>
                <StatusBar
                    translucent={true}
                    backgroundColor={'#324048'}
                    barStyle={'light-content'}
                />

                <View style={{ width: '100%', height: 50, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>

                    <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{this.props.item.name}</Text>

                </View>
                <ScrollView>
                    <View style={{ width: '100%', marginTop: height * 0.1 / 100, }}>
                       <Card style={{ width: '100%',borderRadius:10,padding:5,borderWidth:1,borderColor:'blue',backgroundColor:'blue'}}>
                        <Image
                            style={{ width: '100%', height: 150, }}
                            source={{

                                uri: 'https://us.123rf.com/450wm/alhovik/alhovik1709/alhovik170900031/86481591-breaking-news-background-world-global-tv-news-banner-design.jpg?ver=6'

                            }}

                        ></Image>
                        </Card>

                        <Text 
                        style={{ width: '100%',padding:12,fontSize:18,textAlign:'center'}}>{this.props.item.description}</Text>
                            <TouchableOpacity
                            style={{width: '40%',alignSelf:'center', height: 50,borderRadius:10,
                            borderWidth:1,borderColor:'white',backgroundColor:'black',alignItems: 'center',justifyContent:'center', alignItems: 'center',}}
                         onPress={() => this.onPermission('https://us.123rf.com/450wm/alhovik/alhovik1709/alhovik170900031/86481591-breaking-news-background-world-global-tv-news-banner-design.jpg')}

                           >
                                <Text
                                style={{justifyContent:'center',color:'white',}}>Save Image</Text>

                            </TouchableOpacity>
                            <View style={{width: '100%',padding:7, flexDirection:'row',marginTop:7,}}>
                                <View style={{ width: '30%',alignItems: 'center',justifyContent:'center',}}>
                                    <Text style={{fontSize:14,}}>Billy Bambrough</Text>

                                </View>

                                <TouchableOpacity style={{ width: '50%',alignItems: 'center',justifyContent:'center',}}
                                onPress={() => this.shareDetails()}
                                >
                                    <Image 
                                    style={{width:50,height:50,}}
                                    source={require("../assets/share.png")}></Image>

                                </TouchableOpacity>

                                <View style={{ width: '20%',alignItems: 'center',justifyContent:'center',}}>
                                    <Text style={{fontSize:14,}}>6th July</Text>

                                </View>

                            </View>


                        <Text 
                        style={{ width: '100%',padding:12,fontSize:18,marginTop:10,}}>{this.props.item.description}</Text>

                    </View>
                </ScrollView>


            </SafeAreaView>
        );
    }
}
